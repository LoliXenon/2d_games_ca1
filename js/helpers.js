//HELPER JS FOR GENERAL FUNCTIONS
//
//basic intersection function for checking collision
//takes x and y cordinates and width and height for two objects and compares
function AABBIntersect(ax, ay, aw, ah, bx, by, bw, bh) {
    return ax < bx + bw && bx < ax + aw && ay < by + bh && by < ay + ah;
}

//BULLETS

//function for creating projectiles 
//takes x and y cordinates for origin, y velocity for descent/ascent speed, width and height of bullet,
//color of bullet and id of bullet to determine if it is a player's bullet or an enemies' bullet
function createBullet(x, y, vely, w, h, color, id) {
    this.x = x;
    this.y = y;
    this.vely = vely;
    this.width = w;
    this.height = h;
    this.color = color;
    this.id = id;
}

//move the projectile up/down based on its y velocity
createBullet.prototype.update = function () {
    this.y += this.vely;
};

//CANVAS

//create a canvas and append it to the webpage
function Screen(width, height) {
    this.canvas = document.getElementById("canvas");
    this.canvas.width = this.width = width;
    this.canvas.height = this.height = height;
    this.ctx = this.canvas.getContext("2d");
    var background = new Image();
    background.src = "images/background.png";
    this.ctx.drawImage(background, 10, 10);

    //document.body.appendChild(this.canvas);
};

//clear the canvas
Screen.prototype.clear = function () {
    this.ctx.clearRect(0, 0, this.width, this.height);
};

//draw a sprite on the canvas
Screen.prototype.drawSprite = function (sprite, x, y) {
    this.ctx.drawImage(sprite.image, sprite.x, sprite.y, sprite.w, sprite.h, x, y, sprite.w, sprite.h);
};

//draw a bullet on the canvas
Screen.prototype.drawBullet = function (bullet) {
    this.ctx.fillStyle = bullet.color;
    this.ctx.fillRect(bullet.x, bullet.y, bullet.width, bullet.height);
}

//SPRITES

//sprite definition
//takes an image, an x and y position and a width and height on the spritesheet
function Sprite(image, x, y, w, h) {
    this.image = image;
    this.x = x;
    this.y = y,
        this.w = w;
    this.h = h;
};

//INPUTS

//creates an input handler for handling keypresses
function inputer() {
    this.down = {};
    this.pressed = {};

    var _this = this;

    document.addEventListener("keydown", function (evt) {
        _this.down[evt.keyCode] = true;
    });
    document.addEventListener("keyup", function (evt) {
        delete _this.down[evt.keyCode];
        delete _this.pressed[evt.keyCode];
    });
};

//function to check if a key is held down
inputer.prototype.isDown = function (code) {
    return this.down[code];
};

//function to check if a key has been pressed
inputer.prototype.isPressed = function (code) {
    if (this.pressed[code]) {
        return false;
    }
    else if (this.down[code]) {
        return this.pressed[code] = true;
    }
    return false;
};

