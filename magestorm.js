//MAIN JS FILE FOR GAME EXECUTION
//
//Variable declaration
var display, input, frames, spriteFrame, levelFrame,
   enemySpr, playerSpr,
   enemies, direction, player, fireballs,
   score, hiscore, bgm, fps, lastUpdate, now,
   gameOverYes = false,
   pause = false;

//get the background image
var background = new Image();
background.src = "images/background.png";

//find and get hiscore if it exists, otherwise create (helped by aaron for this)
if (localStorage.length === 0) {
   localStorage.setItem("Hi-Score", '0');
   hiscore = 0;
}
else {
   hiscore = localStorage.getItem("Hi-Score");
}

//Main game setup function
function main() {

   //create a new canvas via helper function
   display = new Screen(598, 600);

   //create the input handler
   input = new inputer();

   bgm = document.getElementById("bgm");
   //create the sprite sheet
   var sheet = new Image();
   sheet.addEventListener("load", function () {
      //load the enemy sprite cells
      enemySpr = [
         [new Sprite(this, 0, 3, 57, 68), new Sprite(this, 238, 3, 57, 68)],
         [new Sprite(this, 0, 151, 42, 57), new Sprite(this, 236, 151, 42, 57)]
      ];
      //load the player sprite cell
      playerSpr = new Sprite(this, 0, 83, 42, 57);

      //initialise and run
      init();
      run();
   });
   //load the sprite sheet image
   sheet.src = "images/spritesheet.png";
};

//Initialisation function for the game
function init() {
   //initialise score
   score = 0;
   //declare frame data
   //initialise
   frames = 0;
   //determine initial enemy sprite animation frame
   spriteFrame = 0;
   //determine update frames per second (seperate from real frames per second)
   levelFrame = 60;

   //initial direction: left to right
   direction = 1;

   //initialise player's data
   player = {
      sprite: playerSpr,
      x: (display.width - playerSpr.w) / 2,
      y: display.height - (30 + playerSpr.h),
      w: playerSpr.w,
      h: playerSpr.h
   }

   //initialise projectile array
   fireballs = [];

   //initialise enemy array
   enemies = [];
   var rows = [0, 0, 1, 1];
   //add enemies to array
   for (var i = 0, len = rows.length; i < len; i++) {
      for (var j = 0; j < 6; j++) {
         var a = rows[i];
         enemies.push({
            sprite: enemySpr[a],
            x: 30 + j * 60,
            y: 30 + i * 60,
            w: enemySpr[a][0].w,
            h: enemySpr[a][0].h
         });
      }
   }
};

//Core game execution loop
function run() {

   var loop = function () {

      update();
      render();
      calculateFPS();
      window.requestAnimationFrame(loop, display.canvas);
   };
   window.requestAnimationFrame(loop, display.canvas);
};

//Update all entities function, and starts music if it is currently not playing
function update() {
   //checks if paused and pauses game if true
   if (pause === false) {
      frames++;

      bgm.play();

      //left arrow - move left
      if (input.isDown(37)) {
         player.x -= 4;
      }

      //right arrow - move right
      if (input.isDown(39)) {
         player.x += 4;
      }
      player.x = Math.max(Math.min(player.x, display.width - (30 + playerSpr.w)), 30);

      //spacebar - fire
      if (input.isPressed(32)) {
         fireballs.push(new createBullet(player.x + 20, player.y, -8, 4, 8, "#F00", 0));
      }

      //move shots up
      for (var i = 0, len = fireballs.length; i < len; i++) {
         var b = fireballs[i];

         b.update();

         //remove bullet when its off screen
         if (b.y + b.height < 0 || b.y > display.height) {
            fireballs.splice(i, 1);
            i--;
            len--;
            continue;
         }

         //remove enemy and bullet if the enemy intersects with a bullet
         for (var j = 0, leng = enemies.length; j < leng; j++) {
            var a = enemies[j];
            if (AABBIntersect(b.x, b.y, b.width, b.height, a.x, a.y, a.w, a.h) && fireballs[i].id == 0) {
               enemies.splice(j, 1);
               j--;
               leng--;
               fireballs.splice(i, 1);
               i--;
               len--;
               score++;
            }
         }

         //if all enemies are dead, clear screen of projectiles and add more enemies,
         //and reduce levelFrame to increase game speed
         if (enemies.length == 0) {
            fireballs = [];
            levelFrame -= 4;
            //make sure it does not go below two
            if (levelFrame < 2) {
               levelFrame = 2;
            }
            var rows = [0, 0, 1, 1];

            //re-add enemies to the array
            for (var i = 0, len = rows.length; i < len; i++) {
               for (var j = 0; j < 6; j++) {
                  var a = rows[i];
                  enemies.push({
                     sprite: enemySpr[a],
                     x: 30 + j * 60,
                     y: 30 + i * 60,
                     w: enemySpr[a][0].w,
                     h: enemySpr[a][0].h
                  });
               }
            }
         }

         //Check to see if the player intersects with any object from the fireball array that originates from an enemy(id = 1)
         if (AABBIntersect(b.x, b.y, b.width, b.height, player.x, player.y, player.w, player.h) && fireballs[i].id === 1) {
            return gameOver();
         }
      }

      //enemy fire shot function
      //random chance to fire, as long as there are enemies in the array
      if (Math.random() < 0.03 && enemies.length > 0) {
         var a = enemies[Math.round(Math.random() * (enemies.length - 1))];

         for (var i = 0, len = enemies.length; i < len; i++) {
            var b = enemies[i];

            //this if statement is designed to pick the enemy in the front row,
            //if an enemy at the back is chosen
            if (AABBIntersect(a.x, a.y, a.w, 100, b.x, b.y, b.w, b.h)) {
               a = b;
            }
         }
         //create new shot and push it to the array
         fireballs.push(new createBullet(a.x + a.w * 0.5, a.y + a.h, 4, 4, 8, "#FFF", 1));
      }


      //update enemy positions
      if (frames % levelFrame === 0) {
         spriteFrame = (spriteFrame + 1) % 2;

         var _max = 0,
            _min = display.width;

         for (var i = 0, len = enemies.length; i < len; i++) {
            var a = enemies[i];
            a.x += 30 * direction;

            //define max width of screen for enemies
            _max = Math.max(_max, a.x + a.w);
            _min = Math.min(_min, a.x);

            //check to see if enemies are on the same level, which results in a game over
            if (enemies[i].y > player.y - 60) {
               return gameOver();
            }

         }

         //check if at edge so enemy can drop line
         if (_max > display.width - 30 || _min < 30) {
            direction *= -1;
            for (var i = 0, len = enemies.length; i < len; i++) {
               enemies[i].x += 30 * direction;
               enemies[i].y += 30;
            }
         }
      }
   }
};

//Game over function
function gameOver() {

   //change variables to stop game
   gameOverYes = true;
   pause = true;

   bgm.pause();

   //call a draw function equivalent
   showGOScreen();

   function showGOScreen() {
      if (gameOverYes === true) {
         display.ctx.font = "48px Helvetica";
         display.ctx.fillStyle = "#FFF";
         display.ctx.fillText('Game Over. Score: ' + score, display.width / 30, display.height / 1.7);
      }

      requestAnimationFrame(showGOScreen);
   }

   if (score >= hiscore) {
      hiscore = score;
      localStorage.setItem("Hi-Score", hiscore);
   }
}

//Function for drawing each object after updating
function render() {
   if (pause === false) {
      //clear previous sprite positions
      display.clear();

      display.ctx.drawImage(background, 10, 10);

      //redraw enemies
      for (var i = 0, len = enemies.length; i < len; i++) {
         var a = enemies[i];
         display.drawSprite(a.sprite[spriteFrame], a.x, a.y);
      }

      //display score in the lower left corner
      document.getElementById('score').innerHTML = 'Score: ' + Math.round(score);
      document.getElementById('hiscore').innerHTML = 'Hiscore: ' + Math.round(hiscore);

      //redraw player
      display.drawSprite(player.sprite, player.x, player.y);

      //redraw projectiles
      display.ctx.save();
      for (var i = 0, len = fireballs.length; i < len; i++) {
         display.drawBullet(fireballs[i]);
      }
      display.ctx.restore();
   }
};

//function to calculate the current framerate of the game
//Help gotten from Aaron for this
function calculateFPS() {
   let now = new Date();
   fps = 1000 / (now - lastUpdate);
   lastUpdate = now;

   document.getElementById('fps').innerHTML = 'FPS: ' + Math.round(fps);
};

//pauses game and music when unfocused
window.onblur = function () {
   pause = true;
   if (pause === true && gameOverYes === false) {
      display.ctx.font = "48px Helvetica";
      bgm.pause();
      display.ctx.fillText("Paused", display.width / 6, display.height / 1.7);
   }
};


//unpauses game after 2.5 seconds after refocusing
window.onfocus = function () {
   setTimeout(function () {
      pause = false;
   }, 2500);
};

//Pause game function. Pressing P while unpaused pauses, while paused unpauses
//Also pauses and unpauses music
window.addEventListener('keydown', function (e) {
   if (e.keyCode === 80 && pause === false && gameOverYes === false) {
      display.ctx.save();
      bgm.pause();
      pause = true;
   }

   else if (e.keyCode === 80 && pause === true && gameOverYes === false) {
      display.ctx.restore();
      bgm.play();
      pause = false;
   }

   //draws paused text on canvas
   if (pause === true && gameOverYes === false) {
      display.ctx.font = "48px Helvetica";
      display.ctx.fillStyle = "#FFF";
      display.ctx.fillText("Paused", display.width / 6, display.height / 1.7);
   }

});

//Reset function
window.addEventListener('keydown', function (e) {
   if (e.keyCode === 82) {
      init();
      gameOverYes = false;
      pause = false;
   }
});

//call main function and start music playback
main();
bgm.play();




